package com.batm.exercise.batmexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatmExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatmExerciseApplication.class, args);
    }

}
