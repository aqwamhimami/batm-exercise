package com.batm.exercise.batmexercise.service;

import com.batm.exercise.batmexercise.entity.TutorialEntity;
import com.batm.exercise.batmexercise.entity.dto.TutorialDTO;

import java.util.List;

public interface TutorialService {

    public TutorialEntity createTutorial(TutorialDTO tutorialDTO);

    public List<TutorialEntity> getAllTutorial();

    public TutorialEntity getTutorialById(int id);

    public TutorialEntity updateTutorialById(TutorialDTO tutorialDTO);

    public void deleteTutorialById(int id);

    public void deleteAllTutorial();

    public List<TutorialEntity> getAllPublishedTutorial();

    public List<TutorialEntity> getTutorialByTitle(String title);

}
