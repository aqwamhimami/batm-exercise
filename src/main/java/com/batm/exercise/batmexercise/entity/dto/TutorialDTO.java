package com.batm.exercise.batmexercise.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TutorialDTO {
    private String id;
    private String description;
    private boolean published;
    private String title;
}
